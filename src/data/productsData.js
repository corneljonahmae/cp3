const productsData = [
    {
        id: "bm001",
        name: "Electric Stand Mixer",
        description: "",
        price: 45000,
        onOffer: true
    },
    {
        id: "bm002",
        name: "Pastry Wheel",
        description: "",
        price: 50000,
        onOffer: true
    },
    {
        id: "bm003",
        name: "Ramekins",
        description: "",
        price: 55000,
        onOffer: true
    },
    {
        id: "bm004",
        name: "Measuring Cups",
        description: "",
        price: 1000,
        onOffer: true
    },
    {
        id: "bm005",
        name: "Rubber Spatula",
        description: "",
        price: 500,
        onOffer: true
    },
    {
        id: "bm006",
        name: "Pastry Brush",
        description: "",
        price: 500,
        onOffer: true
    },
    {
        id: "bm007",
        name: "Parchment Paper",
        description: "",
        price: 55000,
        onOffer: true
    },
    {
        id: "bm008",
        name: "Baking Pan",
        description: "",
        price: 55000,
        onOffer: true
    },
    {
        id: "bm009",
        name: "Whisk",
        description: "",
        price: 55000,
        onOffer: true
    },
    {
        id: "bm010",
        name: "Cooling Rack",
        description: "",
        price: 55000,
        onOffer: true
    },
    {
        id: "bm011",
        name: "Mixing Bowl",
        description: "",
        price: 55000,
        onOffer: true
    },
    {
        id: "bm012",
        name: "Piping Bags",
        description: "",
        price: 55000,
        onOffer: true
    },
    {
        id: "bm013",
        name: "Cake Stand",
        description: "",
        price: 55000,
        onOffer: true
    },

]

export default productsData;

