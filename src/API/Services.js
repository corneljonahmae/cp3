export default function Services() {

	const data = {
        title: "Baking Mama",
        content: "Bake it 'til you make it!",
        destination: "/",
        label: "Shop now!"
    }
    
	return (
		<>
			<h1>Welcome to Homepage</h1>
			<Banner data={data}/>
			<FeaturedProducts />
			{/* <Highlights /> */}
			
		</>
	)
}
