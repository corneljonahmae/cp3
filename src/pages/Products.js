import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

export default function Products() {

	const { user } = useContext(UserContext);

	const [products, setProducts] = useState([]);


	const fetchData = () => {
		fetch(`https://capstone2-ecommerceapi-dasigan.onrender.com/products/all`,{
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			  }
		})
		
		.then(res => res.json())
		.then(data => {
		    
		    console.log(data);
		    setProducts(data);

		});
	}

    useEffect(() => {

		fetchData()

    }, []);

	console.log({products})
	return(
		<>
		
            {
            	(user.isAdmin === true) 
					?
            		<AdminView productsData={products} fetchData={fetchData} />
            		:
            		<UserView productsData={products} />

        	}
        </>
	)
}










