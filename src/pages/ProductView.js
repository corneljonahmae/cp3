import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	
	const { productId } = useParams();
	const { user } = useContext(UserContext);
	
	const navigate = useNavigate();
	const [usersID, setUsersID] = useState('');
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [orderno, setOrderno] = useState(0);

	const checkoutProduct = (productId) => {
		
		fetch(`https://capstone2-ecommerceapi-dasigan.onrender.com/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": `application/json`,
				"Authorization": `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
				productName: name,
				price: price,
				userId: usersID
				
			})
		})
		
		.then(res => res.json())
		.then(data => {
			console.log(data,"here");
			if (data === true) {
				console.log(data);
				Swal.fire({
					title: "Successfully checked this item out.",
					icon: 'success',
					text: "You have successfully  checked this item out."
				});

				navigate("/products");
			
			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		});

	};

	useEffect(()=> {

		fetch(`https://capstone2-ecommerceapi-dasigan.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId]);

	useEffect(()=>{

        fetch(`https://capstone2-ecommerceapi-dasigan.onrender.com/users/details`, {
			method: 'POST',
			headers: {
				"Authorization": `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			// Set the user states values with the user details upon successful login.
			if (typeof data._id !== "undefined") {

				setUsersID(data._id);

			}
        });

    },[])
	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{ user.id !== null ? 
									<Button variant="primary" onClick={() => checkoutProduct(productId)}>Checkout</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Checkout</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
