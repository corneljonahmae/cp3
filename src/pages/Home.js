import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts';

export default function Home() {

	const data = {
        title: "Baking Mama",
        content: "Bake it 'til you make it!",
        destination: "/",
        label: "Shop now!"
    }
    
	return (
		<>
			<h1>Welcome to Homepage</h1>
			<Banner data={data}/>
			<FeaturedProducts />
			{/* <Highlights /> */}
			
		</>
	)
}
