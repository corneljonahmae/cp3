import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({productProp}) {

	const { _id, name, description, price, imgURL} = productProp;
    const imageUrl = imgURL;

	return (
        <div className="col mb-4">
            <Card className="mt-3">
                <Card.Body>
                    <img src={imageUrl} alt="Product Image" className="w-100 card-img-top" />
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP {price}</Card.Text>
                    <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
                </Card.Body>
            </Card>
        </div>
	)
} 


ProductCard.propTypes = {
    
    product: PropTypes.shape({
        
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
