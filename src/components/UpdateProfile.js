import React, { useState } from 'react';
import Swal from 'sweetalert2';

const UpdateProfileForm = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNo] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    const token = localStorage.getItem('token');

    const profileData = {
      firstName,
      lastName,
      mobileNumber,
    };

    fetch('https://capstone2-ecommerceapi-dasigan.onrender.com/users/profile', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(profileData),
    })
    .then(res=>res.json())
    .then((response) => {
      console.log(response.mobileNumber, "Update profile")
        if (response._id) {
          Swal.fire({
            title: "Profile Updated",
            icon: "success",
            text: "Your profile has been successfully udpated!"
        });

          // Profile update was successful, refresh the page
          window.location.reload();
        } else {
          throw new Error('Profile update failed');
        }
      })
      .catch((error) => {
        console.error(error);
        // Handle error here
      });
  };

  return (
    <div className='container'>
        <h2 className='my-4'>Update Profile</h2>
        <form onSubmit={handleSubmit}>
        <div className="mb-3">
            <label htmlFor="firstName" className="form-label">
            First Name
            </label>
            <input
            type="text"
            className="form-control"
            id="firstName"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
            />
        </div>
        <div className="mb-3">
            <label htmlFor="lastName" className="form-label">
            Last Name
            </label>
            <input
            type="text"
            className="form-control"
            id="lastName"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            />
        </div>
        <div className="mb-3">
            <label htmlFor="mobileNumber" className="form-label">
            Mobile Number
            </label>
            <input
            type="text"
            className="form-control"
            id="mobileNumber"
            value={mobileNumber}
            onChange={(e) => setMobileNo(e.target.value)}
            />
        </div>
        <button type="submit" className="btn btn-primary">
            Update Profile
        </button>
        </form>
    </div>
  );
};

export default UpdateProfileForm;

