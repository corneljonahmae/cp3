import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Measuring Cups</h2>
                        </Card.Title>
                        <Card.Text>
                             Baking is all about precision, so having a full set of measuring cups and spoons on hand is a must.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Rubber Spatula</h2>
                        </Card.Title>
                        <Card.Text>
                            Silicone scrapers will stand up to high heat better than rubber ones.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Silicone Brush</h2>
                        </Card.Title>
                        <Card.Text>
                            This tool can be used to grease a pan before pouring in cake batter, to coat the dough with melted butter or egg wash, or to "paint" milk on top of a pie crust.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
		)
}
