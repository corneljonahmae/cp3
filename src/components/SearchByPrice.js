import React, { useState } from 'react';
import ProductCard from './ProductCard';

const SearchByPrice = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [products, setProducts] = useState([]);

  const handleMinPriceChange = (e) => {
    setMinPrice(e.target.value);
  };

  const handleMaxPriceChange = (e) => {
    setMaxPrice(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ minPrice, maxPrice }),
    };

    fetch('https://capstone2-ecommerceapi-dasigan.onrender.com/products/searchByPrice', requestOptions)
      .then((response) => response.json())
      .then((data) => {
        console.log(data)
        setProducts(data.products);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  return (
    <div className="container">
      <h2>Search Products by Price Range</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="minPrice" className="form-label">
            Min Price:
          </label>
          <input
            type="number"
            className="form-control"
            id="minPrice"
            value={minPrice}
            onChange={handleMinPriceChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="maxPrice" className="form-label">
            Max Price:
          </label>
          <input
            type="number"
            className="form-control"
            id="maxPrice"
            value={maxPrice}
            onChange={handleMaxPriceChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Search
        </button>
      </form>
      <h3>Search Results:</h3>
      { products.length === 0 ?  (
        <p>No products found.</p>
      ) : (
        <ul>
          {products.map((product) => (
            
            <ProductCard productProp={product} key={product._id}/>
          ))}
      </ul>
      )}
    </div>
  );
};

export default SearchByPrice;
