import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProduct({ product, fetchData }) {

	const [productId, setProductId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [imgURL, setImgURL] = useState('')
	const [price, setPrice] = useState('')
	const [showEdit, setShowEdit] = useState(false)

	const openEdit = (productId) => {

		if (!productId) {
			console.error('productId is empty or undefined');
			return;
		  }

		const encodedProductId = encodeURIComponent(productId);
		fetch(`https://capstone2-ecommerceapi-dasigan.onrender.com/products/${ encodedProductId }`)
		.then(res => res.json())
		.then(data => {
			console.log(data, "response")
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setImgURL(data.imgURL);
			setPrice(data.price);
			
		})
		.catch((error) => {
			console.error('Error:', error); // Log any errors that occur during the request
		  });
		  setShowEdit(true);
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName('')
		setDescription('')
		setImgURL('')
		setPrice(0)
	}

		const editProduct = (e, productId) => {
			e.preventDefault();

			fetch(`https://capstone2-ecommerceapi-dasigan.onrender.com/products/product/${ productId }`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					imgURL: imgURL,
					price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {
					Swal.fire({
						title: 'Success!',
						icon: 'success',
						text: 'Product Successfully Updated'
					})
					closeEdit();
					fetchData();
					
				} else {
					Swal.fire({
						title: 'Error!',
						icon: 'error',
						text: 'Please try again'
					})
					closeEdit();
					fetchData();
				}
			})
		}


	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}> Edit </Button>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text" 
								value={name} 
								onChange={e => setName(e.target.value)} 
								required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text" 
								value={description} 
								onChange={e => setDescription(e.target.value)} 
								required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Image URL</Form.Label>
							<Form.Control 
								type="text" 
								value={imgURL} 
								onChange={e => setImgURL(e.target.value)} 
								required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number" 
								value={price} 
								onChange={e => setPrice(e.target.value)} 
								required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>
		)
}
