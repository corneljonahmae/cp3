import React, { useState } from 'react';
import Swal from 'sweetalert2';

export default function EditProduct({ product, fetchData }) {

	const [usersID, setUsersID] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

    const encodedUsersId = encodeURIComponent(usersID);

    fetch(`https://capstone2-ecommerceapi-dasigan.onrender.com/users/checkout/${ encodedUsersId }`)
    .then(res => res.json())
    .then(data => {
        console.log(data, "response")
        setUsersID(data._id);
        setName(data.name);
        setPrice(data.price);
        
    })
    .catch((error) => {
        console.error('Error:', error); // Log any errors that occur during the request
        });

	return(
		<>
            <h4>Name:</h4>
            <p>{name}</p>
            <h4>Price:</h4>
            <p>PhP {price}</p>
            <Link className="btn btn-primary" to={`/products/${_id}`}>Buy now</Link>
		</>
		)
}
